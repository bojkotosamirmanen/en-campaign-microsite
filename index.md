---
layout: default
---

## 1. Çfarë mund të bëj konkretisht?

1. Mos harxho para në asnjë prej bizneseve të Samir Manes, si p.sh. SPAR, Neptun, Jumbo, TEG, QTU, Tirana Bank, Aladini, Smart Point, Fashion Group Albania, etj!
2. Bind çdo shoqe/shok dhe të afërm për domosdoshmërinë e bojkotimit të produkteve të Samir Manes!
3. Bashkohu me aktivitetet e Sindikatës së Minatorëve të Bashkuar të Bulqizës! Në pamundësi për të shkuar në Bulqizë, shpërnda në rrjetet sociale deklaratat e tyre!
4. Kontribuo financiarisht, sipas mundësive, për të ndihmuar mbijetesën e minatorëve të pushuar nga puna dhe veprimtaritë e SMBB!
5. Ushtro presionin tënd si qytetar ndaj çdo funksionari publik dhe medie që mund të ketë ndikim në vendosjen e drejtësisë për minatorët e Bulqizës!
6. Organizohu me të tjerë si vetja në vendin e punës, të shkollës apo në lagje për të diskutuar çështjet e punës dhe të demokracisë në vendin tonë.
7. Printo disa sticker-a dhe ngjiti në lagjen ku banon. Nëse ke mundësi, shpërndaji në rrjetet sociale me hashtagun #BojkotoSamirManen.
8. Ndiq faqen Bojkoto Samir Manen në Facebook dhe kontribuo në media sociale me postime #BojkotoSamirManen.

## 2. Kush është solidarizuar me minatorët deri më sot?

Me kauzën e minatorëve të Bulqizës janë solidarizuar mijëra qytetarë, anembanë Shqipërisë. Zëri i tyre ka arritur edhe në Kosovë, ku aktivistë të ndryshëm kanë shkruar parulla denoncuese dhe solidarizuese në muret e Prishtinës dhe Gjilanit. Me ta janë solidarizuar edhe mijëra sindikalistë nga e gjithë bota, nga Kosova, Greqia, tejendanë Evropës e deri në Filipine.

Ata që kanë qëndruar fort me minatorët që nga dita e parë e organizimit të tyre kanë qenë aktivistët e Organizatës Politike. Shtatë prej tyre janë torturuar në mjediset e TEG-ut nga rojet e Samir Manes vetëm sepse shpërndanë fletushka në mbështetje të minatorëve. Tre syresh janë burgosur për tre ditë dhe po ndiqen penalisht po për këtë arsye.

## 3. A është e pashpresë kjo luftë?

Minatorët janë shumë. Qytetarët që po solidarizohen me ta janë shumë e më shumë. Minatorët kanë në dorë çelësat e derës së pasurimit të kryeoligarkut. Nëpërmjet grevave dhe protestave, ata kanë mundësi ta thyejnë atë që po u merr jetën dhe shëndetin. Ne, si qytetarë, kemi shumëçka në dorë. Duke ndërgjegjësuar njëri-tjetrin, shpërndarë thirrjen për bojkot dhe u organizuar në një front të gjerë shoqëror kemi mundësinë që jo vetëm të ndihmojmë në fitoren e minatorëve, por edhe të shkatërrojmë themelet e oligarkisë. Shpresa jo vetëm ekziston, por është e madhe.

## 4. Përse duhet të më përkasë mua kjo kauzë, kur unë nuk jam minator?

Së bashku me minatorët, ne jemi pjesë e të njëjtës shoqëri. Shumëkush prej nesh është në marrëdhënie pune, ku pronarët apo drejtuesit nuk na japin hakun dhe na shkelin të drejtat. Fitorja e minatorëve do të ishte fitore për sindikalizmin. Ajo do të na e lehtësonte të gjithëve ne organizimin në vendet e punës. Nga ana tjetër, si qytetarë ne kemi të drejtën të jetojmë në një rend ligjor dhe demokratik. Kapja e shtetit dhe e mediave nga oligarkët, si p.sh. Samir Mane, na shndërron në nënshtetas kokulur të një pakice të pushtetshme dhe të pasur. Në një shtet oligarkik nuk është e largët dita kur dikush do të trokasë edhe në derën tonë, për të na grabitur edhe më shumë, hequr lirinë dhe marrë dinjitetin.

## 5. Përse duhen bojkotuar produktet e Samir Manes?

Kjo është mënyra më efektive e ushtrimit të presionit qytetar ndaj Samir Manes. Si biznesmen, atij i interesojnë vetëm paratë. Duke e masivizuar bojkotimin e mallrave dhe shërbimeve të të gjitha bizneseve të tij, ne mund t’ia rrisim atij koston ekonomike aq shumë, saqë çdo përfitim që ai nxjerr nga shtypja dhe kërcënimi i minatorëve do të ishte i paleverdisshëm.

## 6. Përse mediat po heshtin?

Shumica dërrmuese e mediave financohen nga oligarkët drejtpërdrejt ose nëpërmjet reklamave. Pronari i Albchrome-it, Samir Mane, është oligarku më i pasur dhe i pushtetshëm. Përmes pagesave të drejtpërdrejta ose kërcënimit të tërheqjes së reklamave, ai ua mbyll gojën thuajse të gjitha redaksive. Edhe më keq, ndikimi i tij i paligjshëm ndihet edhe në kronikat propagandistike të televizioneve kombëtare si Klan dhe Top Channel.

## 7. Çfarë ka bërë tjetër kompania AlbChrome?

Kompania Albchrome ka shkelur edhe pikën 7 të nenin 181 të Kodit të Punës, duke i penguar sindikalistët të ushtrojnë veprimtarinë e tyre informuese, ndërgjegjësuese dhe organizative brenda mjediseve të punës. Nga ana tjetër, kompania Albchrome vazhdon të shkelë nenet 163 dhe 164 të Kodit të Punës, duke mos i nisur bisedimet për kontratën kolektive me sindikatën që ka më shumë anëtarë, SMBB. Po ashtu, gjatë grevës së nëntor-dhjetorit 2019, drejtuesit e kompanisë kanë shkelur nenin 264 të Kodit Penal, i cili e ndalon rreptësisht detyrimin e punonjësve për të mos marrë pjesë në grevë.

## 8. Çfarë shkeljesh ka bërë kompania AlbChrome duke pushuar minatorët nga puna?

Kompania Albchrome ka shkelur nenin 181, sidomos në pikat 3 dhe 4, të Kodit të Punës. Sipas tyre punëdhënësi nuk mund t’i diskriminojë punëtorët për shkak të aktivitetit të tyre sindikalist dhe nuk mund t’i heqë nga puna përfaqësuesit e një sindikate pa pëlqimin e vetë sindikatës.

## 9. A ka bërë ai investime në Bulqizë?

Samir Mane deklaron se ka investuar në galeri dhe në infrastrukturën e Bulqizës. Sidoqoftë nuk ka asnjë burim të pavarur institucional dhe mediatik që do të na tregonte se për çfarë shifrash bëhet fjalë. Ajo që dihet nga minatorët është se investimet në galeri janë bërë nga fitimet e nxjerra nga shfrytëzimi i rëndë i minatorëve, ndërkohë që “bamirësitë” e tij për Bulqizën janë tejet simbolike. Interesant është fakti se për të hapur disa nivele më të thella në minierë, Albchrome-i i Samir Manes ka kontraktuar një kompani kineze. Ndryshe nga kompanitë përendimore të thellimit të puseve minerare, kompanitë kineze shquhen për punime jocilësore, që vendosin në rrezik jetën e minatorëve, por kanë kosto më të ulët për porositësit.
## 10. Çfarë zotëron tjetër Samir Mane?

Samir Mane ka aq shumë pasuri saqë mund të blejë vendimet dhe qëndrimet e çdo partie politike, të qeverisë dhe opozitës, madje edhe të institucioneve gjyqësore. Ai ushtron hegjemoni të plotë mediatike, që duket jo vetëm nga kronikat propagandistike që e mburrin si bamirës dhe investitor, por edhe në heshtjen thuajse të plotë të mediave ndaj grevës dhe rezistencës së minatorëve të Bulqizës.
## 11. Kush është Samir Mane?

Samir Mane është oligarku më i pasur dhe i pushtetshëm në Shqipëri. Si çdo oligark tjetër, pasuria e tij është ngritur përmes ortakërisë okulte dhe korruptive me qeveritë e njëpasnjëshme. Ndryshe nga sipërmarrësit modernë të vendeve të përparuara, Samir Mane investon në sektorë joproduktivë dhe ka nevojë për favoret dhe koncesionet e një shteti të korruptuar.
## 12. Kur këto çështje mund të zgjidhen nëpërmjet gjykatës, përse duhen sindikatat?

Në Shqipëri shumica e gjykatësve janë të korruptuar ose të nënshtruar politikisht. Kështu, nuk është çudi që ata ta shkelin ligjin duke i dhënë të drejtë njeriut më të pasur në Shqipëri, Samir Manes. Po ashtu, edhe një vendim i drejtë do shumë kohë për t’u marrë, ndërkohë që sindikalistët janë pa punë e pa të ardhura. Sidoqoftë, lufta sindikaliste nuk është vetëm në emër të të drejtave që u njeh ligji aktual, por edhe në emër të të drejtave të reja për të cilat nevojiten ndryshime ligjore. Rruga më e sigurt dhe e shpejtë për t’i arritur të gjitha këto është veprimtaria intensive sindikaliste, ku ndryshimi vjen nga poshtë.
## 13. A ka mundësi që katër minatorët e pushuar të kthehen në punë?

Minatorët mund të kthehen në punë sipas rrugës së shpejtë dhe tejet frymëzuese, ose sipas një rruge të gjatë dhe më pak frymëzuese. Rruga e parë kalon nga solidariteti i qindra minatorëve që nëpërmjet peticioneve, protestave dhe grevave do ta detyrojnë kompaninë të zmbrapset nga paligjshmëria. Rifutja e 4 minatorëve në galeri nga aktivizmi i fortë i sindikalistëve do të vendoste një precedent të rëndësishëm dhe do të tregonte forcën e papërballueshme të punëtorëve të bashkuar. Rruga e dytë është ndjekja e procedurave të gjata ligjore, ku gjykata duhet të konstatojë paligjshmërinë flagrante të pushimit nga puna të katër përfaqësuesve sindikalë, sidomos për shkak të veprimtarisë së tyre sindikaliste.
## 14. Çfarë ka ndodhur tjetër me punëtorët e Sindikatës së Minatorëve të Bashkuar të Bulqizës?

Anëtarëve të SMBB u bëhet presion që të largohen nga sindikata e tyre, të anëtarësohen në sindikatën formale që i shërben kompanisë, të mos solidarizohen me shokët e pushuar nga puna dhe të pranojnë diktatin e kompanisë edhe kur ai u rrezikon jetën dhe shëndetin.

## 15. Përse është kaq e rëndësishme të ketë sindikatë?

Sindikata e Minatorëve të Bashkuar të Bulqizës jo vetëm përfaqëson shumicën e minatorëve, por edhe është e organizuar sipas parimeve mirëfilli demokratike. Ajo shërben si formë e organizimit dhe emancipimit të minatorëve dhe ka për funksion të ushtrojë presion ndaj kompanisë dhe institucioneve publike që të respektohen të drejtat e minatorëve. Pa sindikata dhe lëvizje punëtore nuk ka as të drejta, as demokraci të vërtetë.

## 16. A ka pasur sindikatë tjetër në Bulqizë?

Prej shumë vitesh në Bulqizë nuk ka pasur asnjë sindikatë reale. E vetmja që ka ekzistuar formalisht ka shërbyer dhe shërben thjesht si vegël e kompanisë, duke u bërë presion punëtorëve dhe luftuar kundër të drejtave të tyre. Sindikata formale nuk ka thuajse fare anëtarë dhe mbahet vetëm si vegël e kompanisë. Përfaqësuesit e asaj sindikate kanë privilegjin të paguhen pa hyrë në galeri.

## 17. Çfarë kërkojnë minatorët?

1. Negocimin e kontratës së re kolektive me SMBB, si përfaqësuesja e numrit më të madh të minatorëve.
2. Rritje page.
3. Rishikim norme.
4. Njohje të vështirësisë dhe vjetërsisë.
5. Dëmshpërblim për aksidentet.
6. Ndaljen e presioneve nga kompania.
7. Kthimin në punë të 4 përfaqësuesve sindikalë, të pushuar paligjshmërisht.

## 18. Çfarë po ndodh në Bulqizë?

Në tetor të vitit 2019, minatorët e kompanisë AlbChrome formuan Sindikatën e Minatorëve të Bashkuar të Bulqizës, sipas të gjitha procedurave të kërkuara nga Kodi i Punës. Kryetar i sindikatës u zgjodh Elton Debreshi. Trupa më e rëndësishme drnejtuese është Këshilli Sindikal, i përbërë nga 21 anëtarë. Kryetari dhe anëtarët e Këshillit mund të largohen nga detyra kurdoherë që shumica u heq përkrahjen. Sindikata u njoh nga Gjykata e Rrethit Tiranë më datë 15 tetor 2019. Katër ditë pas shpalljes publike të Sindikatës së Minatorëve të Bashkuar të Bulqizës (SMBB) në qendër të Bulqizës, kompania AlbChrome pushoi nga puna kryetarin Elton Debreshi, me motivacionin e shkeljes së disiplinës në punë. Pas pak javësh janë hequr nga puna edhe tre minatorë të tjerë: Beqir Duriçi, Behar Gjimi dhe Ali Gjeta, të gjithë pjesë e Këshillit të SMBB. Që nga ajo ditë, kompania ka vazhduar të kërcënojë me heqje nga puna të gjithë minatorët të cilët janë të angazhuar në SMBB.
