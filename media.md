---
---

### Mbulim mediatik

- EN: [The difficult birth of an independent labour movement in Albania](http://column.global-labour-university.org/2020/01/the-difficult-birth-of-independent.html)
- FR: [L’oligarchie du chrome ne veut pas de syndicat indépendant](https://www.courrierdesbalkans.fr/Albanie-Bulqize-l-oligarchie-ne-veut-pas-de-syndicat-independant)
- IT: [Dalle viscere della terra allo scoperto: la lotta dei minatori albanesi](https://www.infoaut.org/approfondimenti/dalle-viscere-della-terra-allo-scoperto-la-lotta-dei-minatori-albanesi)
- EN: [Miner earthquakes in Bulqiza](https://kosovotwopointzero.com/en/miner-earthquakes-in-bulqiza/)
- EN: [Unearthing stories from Bulqiza’s underground](https://kosovotwopointzero.com/en/unearthing-stories-from-bulqizas-underground/)
- AL: [“Pse e keni kapur me Samir Manen?”](https://teza11.org/pse-e-keni-kapur-me-samir-manen/)
- AL: [Kur përbindëshat bëhen qesharakë](https://teza11.org/kur-perbindeshat-behen-qesharake/)
