---
---


### Çfarë mund të bëj konkretisht

Në tetor të vitit 2019, minatorët e kompanisë AlbChrome formuan Sindikatën e Minatorëve të Bashkuar të Bulqizës, sipas të gjitha procedurave të kërkuara nga Kodi i Punës. Kryetar i sindikatës u zgjodh Elton Debreshi. Trupa më e rëndësishme drnejtuese është Këshilli Sindikal, i përbërë nga 21 anëtarë. Kryetari dhe anëtarët e Këshillit mund të largohen nga detyra kurdoherë që shumica u heq përkrahjen. Sindikata u njoh nga Gjykata e Rrethit Tiranë më datë 15 tetor 2019. Katër ditë pas shpalljes publike të Sindikatës së Minatorëve të Bashkuar të Bulqizës (SMBB) në qendër të Bulqizës, kompania AlbChrome pushoi nga puna kryetarin Elton Debreshi, me motivacionin e shkeljes së disiplinës në punë. Pas pak javësh janë hequr nga puna edhe tre minatorë të tjerë: Beqir Duriçi, Behar Gjimi dhe Ali Gjeta, të gjithë pjesë e Këshillit të SMBB. Që nga ajo ditë, kompania ka vazhduar të kërcënojë me heqje nga puna të gjithë minatorët të cilët janë të angazhuar në SMBB.

* Facebook: [Bojkoto Samir Manen!](https://www.facebook.com/bojkotosamirmanen/)
* Facebook: [Sindikata e Minatorëve të Bashkuar të Bulqizës](https://www.facebook.com/MinatoretSMBB/)
